---DESCRIPTION---

This submodule adds a log inspector to the Rijksoverheid Cookie Opt-in module.

The module requires the Rijksoverheid Cookie Opt-in module to be installed and
logging to the Drupal database to be enabled in the Rijksoverheid Cookie Opt-in
module settings.

The module provides the following features:

 * Browse the cookie log.
 * Filter the cookie log.
 * Export the cookie log to a csv file.

Development of this module is sponsored by LimoenGroen.

---INSTALLATION---

Prerequisites:

* Installed Rijksoverheid Cookie Opt-in

Installation steps:

1 > Download the Rijkoverheid Cooke Opt-in module from drupal.org.
2 > Extract the module in the contrbuted modules directory.
3 > Enable the Rijsoverheid Cookie Opt-in Log module at http://www.example.com/
    admin/modules
4 > Set the permissions at http://www.example.com/admin/people/permissions
